﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABRSMPracticalTestTool.Data
{
    public static class EnumUtil
    {
	    public enum KeyName
	    {
			A,
			A_FLAT,
			A_SHARP,
			B,
			B_FLAT,
			C,
			C_SHARP,
			D,
			D_FLAT,
			D_SHARP,
			E,
			E_FLAT,
			F,
			F_SHARP,
			G,
			G_FLAT,
			G_SHARP,
	    }

	    public enum BlackKeys
	    {
			C_SHARP,
			D_FLAT,
			D_SHARP,
			E_FLAT,
			F_SHARP,
			G_FLAT,
			G_SHARP,
			A_FLAT,
			A_SHARP,
			B_FLAT
	    }

        public enum ScaleMode
        {
            MAJOR,
            NATURAL_MINOR,
            MELODIC_MINOR,
            HARMONIC_MINOR,
            CONTRARY_MOTION,
            CHROMATIC,
			WHOLE_TONE
        }

	    public enum ChordType
	    {
			POWER,
			TRIAD,
			BROKEN,
			ARPEGGIO,
			SEVENTHS,
			DOMINANT_SEVENTHS,
			DIMINISHED_SEVENTHS
	    }

        public enum HandDirection
        {
            ASCENDING_DESCENDING,
            CONTRARY
        }

	    public enum Articulation
	    {
			LEGATO,
			STACCATO
	    }

        public enum Hands
        {
            LEFT,
            RIGHT,
            SEPARATE,
            TOGETHER,
        }
    }
}
