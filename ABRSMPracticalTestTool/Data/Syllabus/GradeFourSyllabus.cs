﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Model;

namespace ABRSMPracticalTestTool.Data.Syllabus
{
    public class GradeFourSyllabus : IExamSyllabus
    {
	    public Scales Scales { get; set; }
	    public Chords Chords { get; set; }

	    public GradeFourSyllabus()
	    {
			Scales = new Scales(new List<IScale>
			{
				new MajorScale(EnumUtil.KeyName.B, 2, 0),
				new MajorScale(EnumUtil.KeyName.B_FLAT, 2, 0),
				new MajorScale(EnumUtil.KeyName.E_FLAT, 2, 0),
				new MajorScale(EnumUtil.KeyName.A_FLAT, 2, 0),
				new MajorScale(EnumUtil.KeyName.D_FLAT, 2, 0),
				new MelodicMinorScale(EnumUtil.KeyName.C_SHARP, 2, 0),
				new HarmonicMinorScale(EnumUtil.KeyName.C_SHARP, 2, 0),
				new MelodicMinorScale(EnumUtil.KeyName.G_SHARP, 2, 0),
				new HarmonicMinorScale(EnumUtil.KeyName.G_SHARP, 2, 0),
				new MelodicMinorScale(EnumUtil.KeyName.C, 2, 0),
				new HarmonicMinorScale(EnumUtil.KeyName.C, 2, 0),
				new MelodicMinorScale(EnumUtil.KeyName.F, 2, 0),
				new HarmonicMinorScale(EnumUtil.KeyName.F, 2, 0),
				new ContraryMotionScale(EnumUtil.KeyName.F, EnumUtil.ScaleMode.MAJOR, 2, 0),
				new ContraryMotionScale(EnumUtil.KeyName.E_FLAT, EnumUtil.ScaleMode.MAJOR, 2, 0),
				new ContraryMotionScale(EnumUtil.KeyName.D, EnumUtil.ScaleMode.HARMONIC_MINOR, 2, 0),
				new ContraryMotionScale(EnumUtil.KeyName.C, EnumUtil.ScaleMode.HARMONIC_MINOR, 2, 0),
				new ChromaticScale(2, 0)
			});

		    Chords = new Chords(new List<IChord>
		    {
			    new Arpeggio(EnumUtil.KeyName.B, EnumUtil.ScaleMode.MAJOR, 2),
			    new Arpeggio(EnumUtil.KeyName.B_FLAT, EnumUtil.ScaleMode.MAJOR, 2),
			    new Arpeggio(EnumUtil.KeyName.E_FLAT, EnumUtil.ScaleMode.MAJOR, 2),
			    new Arpeggio(EnumUtil.KeyName.A_FLAT, EnumUtil.ScaleMode.MAJOR, 2),
			    new Arpeggio(EnumUtil.KeyName.D_FLAT, EnumUtil.ScaleMode.MAJOR, 2),
			    new Arpeggio(EnumUtil.KeyName.C_SHARP, EnumUtil.ScaleMode.NATURAL_MINOR, 2),
			    new Arpeggio(EnumUtil.KeyName.G_SHARP, EnumUtil.ScaleMode.NATURAL_MINOR, 2),
			    new Arpeggio(EnumUtil.KeyName.C, EnumUtil.ScaleMode.NATURAL_MINOR, 2),
			    new Arpeggio(EnumUtil.KeyName.F, EnumUtil.ScaleMode.NATURAL_MINOR, 2),
		    });
		}
    }
}
