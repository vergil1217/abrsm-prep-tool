﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABRSMPracticalTestTool.Model
{
    public class Scales : IExamComponent
    {
        public List<IScale> GradeScales { get; set; }

        public Scales() : this(new List<IScale>()) { }
        public Scales(List<IScale> gradeScales)
        {
            GradeScales = gradeScales;
        }
    }
}
