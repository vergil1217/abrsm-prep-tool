﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABRSMPracticalTestTool.Model
{
	public class Chords : IExamComponent
	{
		public List<IChord> GradeChords { get; set; }

		public Chords() : this(new List<IChord>())
		{
			
		}

		public Chords(List<IChord> gradeChords)
		{
			GradeChords = gradeChords;
		}
	}
}
