﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
    public class ComponentHands : IExamComponent
    {
	    public Dictionary<IExamComponent, EnumUtil.Hands> ComponentHandsDictionary;

	    public ComponentHands(IExamComponent component, EnumUtil.Hands hands)
	    {
		    ComponentHandsDictionary = new Dictionary<IExamComponent, EnumUtil.Hands> {{component, hands}};
	    }
    }
}
