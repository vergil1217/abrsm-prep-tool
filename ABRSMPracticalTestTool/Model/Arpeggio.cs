﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
	public class Arpeggio : IChord
	{
		public EnumUtil.KeyName KeyName { get; set; }
		public EnumUtil.ScaleMode ScaleMode { get; set; }
		public EnumUtil.ChordType ChordType { get; set; }
		public EnumUtil.HandDirection HandDirection { get; set; }
		public int Octaves { get; set; }

		public Arpeggio(EnumUtil.KeyName keyName, EnumUtil.ScaleMode scaleMode, int octaves)
		{
			KeyName = keyName;
			ScaleMode = scaleMode;
			ChordType = EnumUtil.ChordType.ARPEGGIO;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
		}
	}
}
