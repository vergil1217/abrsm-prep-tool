﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
	public class MajorScale : IScale
	{
		public EnumUtil.KeyName KeyName { get; set; }
		public EnumUtil.ScaleMode ScaleMode { get; set; }
		public EnumUtil.HandDirection HandDirection { get; set; }
		public int Octaves { get; set; }
		public int IntervalApart { get; set; }
		public int FingerIntervalApart { get; set; }
		public EnumUtil.Articulation Articulation { get; set; }

		public MajorScale(EnumUtil.KeyName keyName, int octaves, int intervalApart)
		{
			KeyName = keyName;
			ScaleMode = EnumUtil.ScaleMode.MAJOR;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = 0;
		}

		public MajorScale(EnumUtil.KeyName keyName, int octaves, int intervalApart, int fingerIntervalApart)
		{
			KeyName = keyName;
			ScaleMode = EnumUtil.ScaleMode.MAJOR;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = fingerIntervalApart;
		}

		public MajorScale(EnumUtil.KeyName keyName, int octaves, int intervalApart, int fingerIntervalApart, EnumUtil.Articulation articulation)
		{
			KeyName = keyName;
			ScaleMode = EnumUtil.ScaleMode.MAJOR;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = fingerIntervalApart;
			Articulation = articulation;
		}
	}
}
