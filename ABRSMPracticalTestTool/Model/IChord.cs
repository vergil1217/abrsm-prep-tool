﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
	public interface IChord
	{
		EnumUtil.KeyName KeyName { get; set; }
		EnumUtil.ScaleMode ScaleMode { get; set; }
		EnumUtil.ChordType ChordType { get; set; }
		EnumUtil.HandDirection HandDirection { get; set; }
		int Octaves { get; set; }
	}
}
