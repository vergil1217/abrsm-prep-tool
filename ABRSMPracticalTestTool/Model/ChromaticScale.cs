﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
	public class ChromaticScale : IScale
	{
		public EnumUtil.KeyName KeyName { get; set; }
		public EnumUtil.ScaleMode ScaleMode { get; set; }
		public EnumUtil.HandDirection HandDirection { get; set; }
		public int Octaves { get; set; }
		public int IntervalApart { get; set; }
		public int FingerIntervalApart { get; set; }
		public EnumUtil.Articulation Articulation { get; set; }

		public ChromaticScale(int octaves, int intervalApart)
		{
			ScaleMode = EnumUtil.ScaleMode.CHROMATIC;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = 0;
		}

		public ChromaticScale(int octaves, int intervalApart, int fingerIntervalApart)
		{
			ScaleMode = EnumUtil.ScaleMode.CHROMATIC;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = fingerIntervalApart;
		}

		public ChromaticScale(int octaves, int intervalApart, EnumUtil.Articulation articulation)
		{
			ScaleMode = EnumUtil.ScaleMode.CHROMATIC;
			HandDirection = EnumUtil.HandDirection.ASCENDING_DESCENDING;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = FingerIntervalApart;
			Articulation = articulation;
		}
	}
}
