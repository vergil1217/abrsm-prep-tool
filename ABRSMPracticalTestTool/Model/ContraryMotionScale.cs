﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
	public class ContraryMotionScale : IScale
	{
		public EnumUtil.KeyName KeyName { get; set; }
		public EnumUtil.ScaleMode ScaleMode { get; set; }
		public EnumUtil.HandDirection HandDirection { get; set; }
		public int Octaves { get; set; }
		public int IntervalApart { get; set; }
		public int FingerIntervalApart { get; set; }
		public EnumUtil.Articulation Articulation { get; set; }

		public ContraryMotionScale(EnumUtil.KeyName keyName, EnumUtil.ScaleMode scaleMode, int octaves, int intervalApart)
		{
			KeyName = keyName;
			ScaleMode = scaleMode;
			HandDirection = EnumUtil.HandDirection.CONTRARY;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = 0;
		}

		public ContraryMotionScale(EnumUtil.KeyName keyName, EnumUtil.ScaleMode scaleMode, int octaves, int intervalApart, int fingerIntervalApart)
		{
			KeyName = keyName;
			ScaleMode = scaleMode;
			HandDirection = EnumUtil.HandDirection.CONTRARY;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = fingerIntervalApart;
		}

		public ContraryMotionScale(EnumUtil.KeyName keyName, EnumUtil.ScaleMode scaleMode, int octaves, int intervalApart, int fingerIntervalApart, EnumUtil.Articulation articulation)
		{
			KeyName = keyName;
			ScaleMode = scaleMode;
			HandDirection = EnumUtil.HandDirection.CONTRARY;
			Octaves = octaves;
			IntervalApart = intervalApart;
			FingerIntervalApart = fingerIntervalApart;
			Articulation = articulation;
		}
	}
}
