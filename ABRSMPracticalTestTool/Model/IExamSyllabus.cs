﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABRSMPracticalTestTool.Model
{
    public interface IExamSyllabus
    {
		Scales Scales { get; set; }
		Chords Chords { get; set; }
    }
}
