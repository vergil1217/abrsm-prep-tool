﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Model
{
	public interface IScale
	{
		EnumUtil.KeyName KeyName { get; set; }
		EnumUtil.ScaleMode ScaleMode { get; set; }
		EnumUtil.HandDirection HandDirection { get; set; }
		int Octaves { get; set; }
		int IntervalApart { get; set; } // Interval apart between hands
		int FingerIntervalApart { get; set; } // Interval apart between fingers of one hand
		EnumUtil.Articulation Articulation { get; set; }
	}
}
