﻿namespace ABRSMPracticalTestTool.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.labelSelectedGrade = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.labelSelectedScales = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tableLayoutSyllabus = new System.Windows.Forms.TableLayoutPanel();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(241, 36);
			this.label1.TabIndex = 0;
			this.label1.Text = "Selected Grade:";
			// 
			// labelSelectedGrade
			// 
			this.labelSelectedGrade.AutoSize = true;
			this.labelSelectedGrade.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSelectedGrade.ForeColor = System.Drawing.Color.White;
			this.labelSelectedGrade.Location = new System.Drawing.Point(259, 9);
			this.labelSelectedGrade.Name = "labelSelectedGrade";
			this.labelSelectedGrade.Size = new System.Drawing.Size(0, 36);
			this.labelSelectedGrade.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Location = new System.Drawing.Point(12, 77);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(321, 36);
			this.label2.TabIndex = 2;
			this.label2.Text = "Selected Minor Mode:";
			// 
			// labelSelectedScales
			// 
			this.labelSelectedScales.AutoSize = true;
			this.labelSelectedScales.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSelectedScales.ForeColor = System.Drawing.Color.White;
			this.labelSelectedScales.Location = new System.Drawing.Point(339, 77);
			this.labelSelectedScales.Name = "labelSelectedScales";
			this.labelSelectedScales.Size = new System.Drawing.Size(65, 36);
			this.labelSelectedScales.TabIndex = 3;
			this.labelSelectedScales.Text = "111";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Location = new System.Drawing.Point(12, 223);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(354, 36);
			this.label3.TabIndex = 4;
			this.label3.Text = "Current Grade Syllabus:";
			// 
			// tableLayoutSyllabus
			// 
			this.tableLayoutSyllabus.AutoSize = true;
			this.tableLayoutSyllabus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutSyllabus.ColumnCount = 3;
			this.tableLayoutSyllabus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutSyllabus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutSyllabus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutSyllabus.Location = new System.Drawing.Point(18, 284);
			this.tableLayoutSyllabus.Name = "tableLayoutSyllabus";
			this.tableLayoutSyllabus.RowCount = 2;
			this.tableLayoutSyllabus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutSyllabus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutSyllabus.Size = new System.Drawing.Size(20, 0);
			this.tableLayoutSyllabus.TabIndex = 5;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(1422, 1103);
			this.Controls.Add(this.tableLayoutSyllabus);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.labelSelectedScales);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.labelSelectedGrade);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.ShowIcon = false;
			this.Text = "ABRSM Piano Practical Prep Tool";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelSelectedGrade;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelSelectedScales;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutSyllabus;
	}
}

