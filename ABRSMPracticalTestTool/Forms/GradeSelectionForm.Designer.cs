﻿namespace ABRSMPracticalTestTool.Forms
{
    partial class GradeSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.cbGradeSelection = new System.Windows.Forms.ComboBox();
			this.bSelectGrade = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(12, 62);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(384, 37);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select your current grade:";
			// 
			// cbGradeSelection
			// 
			this.cbGradeSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbGradeSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbGradeSelection.FormattingEnabled = true;
			this.cbGradeSelection.Location = new System.Drawing.Point(402, 59);
			this.cbGradeSelection.Name = "cbGradeSelection";
			this.cbGradeSelection.Size = new System.Drawing.Size(147, 45);
			this.cbGradeSelection.TabIndex = 1;
			// 
			// bSelectGrade
			// 
			this.bSelectGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.bSelectGrade.Location = new System.Drawing.Point(210, 156);
			this.bSelectGrade.Name = "bSelectGrade";
			this.bSelectGrade.Size = new System.Drawing.Size(186, 55);
			this.bSelectGrade.TabIndex = 2;
			this.bSelectGrade.Text = "Confirm";
			this.bSelectGrade.UseVisualStyleBackColor = true;
			this.bSelectGrade.Click += new System.EventHandler(this.bSelectGrade_Click);
			// 
			// GradeSelectionForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(584, 223);
			this.Controls.Add(this.bSelectGrade);
			this.Controls.Add(this.cbGradeSelection);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GradeSelectionForm";
			this.ShowIcon = false;
			this.Text = "Grade Selection";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GradeSelectionForm_FormClosing);
			this.Load += new System.EventHandler(this.GradeSelectionForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbGradeSelection;
        private System.Windows.Forms.Button bSelectGrade;
    }
}