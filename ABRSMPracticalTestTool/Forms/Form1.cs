﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ABRSMPracticalTestTool.Data;
using ABRSMPracticalTestTool.Data.Syllabus;
using ABRSMPracticalTestTool.Model;
using Button = System.Windows.Forms.Button;

namespace ABRSMPracticalTestTool.Forms
{
    public partial class Form1 : Form
    {
        public int SelectedGrade { get; set; }
		public IExamSyllabus ExamSyllabus { get; set; }
		public EnumUtil.ScaleMode SelectedMinorScaleMode { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var gradeSelectionForm = new GradeSelectionForm(this);
            gradeSelectionForm.ShowDialog();

            var scalesSetupForm = new ScalesSetup(this);
            scalesSetupForm.ShowDialog();

	        GetGradeSyllabus(SelectedGrade);
			
            PopulateInformationText();

	        PopulateSyllabusInformation();
        }

	    private void GetGradeSyllabus(int grade)
	    {
		    switch (grade)
		    {
				case 1:
					break;
				case 2:
					break;
				case 3:
					break;
				case 4:
					ExamSyllabus = new GradeFourSyllabus();
					break;
				case 5:
					break;
				case 6:
					break;
				case 7:
					break;
				case 8:
					break;
				default:
					break;
		    }
	    }

        private void PopulateInformationText()
        {
	        labelSelectedGrade.Text = SelectedGrade.ToString();

			switch (SelectedMinorScaleMode)
			{
				case EnumUtil.ScaleMode.NATURAL_MINOR:
					labelSelectedScales.Text = "Natural Minor";
					break;
				case EnumUtil.ScaleMode.MELODIC_MINOR:
					labelSelectedScales.Text = "Melodic Minor";
					break;
				case EnumUtil.ScaleMode.HARMONIC_MINOR:
					labelSelectedScales.Text = "Harmonic Minor";
					break;
				default:
					labelSelectedScales.Text = "How did this happen?";
					break;
			}
		}

	    private void PopulateSyllabusInformation()
	    {
			// Scales
		    var scaleList = ExamSyllabus.Scales.GradeScales.Select(x => x.GetType().ToString()).Distinct();

		    var selectedMinorToRemove = scaleList.Where(x => x.Contains("MinorScale") && !x.ToLower().Contains(SelectedMinorScaleMode.ToString().ToLower().Split('_')[0]));

		    var finalSyllabusList = scaleList.Except(selectedMinorToRemove).ToList();

		    for (var i = 0; i < finalSyllabusList.Count; i++)
		    {
				// Display label
			    var label = new Label
			    {
				    Text = Regex.Replace(finalSyllabusList[i].Split('.').Last(), "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 "),
				    ForeColor = Color.White,
				    Font = new Font("Arial", 12),
				    AutoSize = true,
				    Padding = new Padding(10, 10, 10, 10)
			    };
			    tableLayoutSyllabus.Controls.Add(label, 0, i);

				// Button
			    var button = new Button
			    {
				    AutoSize = true,
					Text = $"Start {label.Text} Module",
					Font = new Font("Arial", 12),
					BackColor = Color.AliceBlue
			    };

				button.Click += StartModuleOnClick;

				tableLayoutSyllabus.Controls.Add(button, 1, i);

				// Hidden label
			    var hiddenLabel = new Label
			    {
					Visible = false,
					Text = finalSyllabusList[i]
				};

				tableLayoutSyllabus.Controls.Add(hiddenLabel, 2, i);
		    }
	    }

	    private void StartModuleOnClick(object sender, EventArgs eventArgs)
	    {
		    var rowOfButton = tableLayoutSyllabus.GetRow((Control) sender);

		    var hiddenLabel = (Label)GetAnyControlAt(tableLayoutSyllabus, 2, rowOfButton);

			//TODO: Add Module Form here
	    }

	    private static Control GetAnyControlAt(TableLayoutPanel panel, int column, int row)
	    {
		    foreach (Control control in panel.Controls)
		    {
			    var cellPosition = panel.GetCellPosition(control);
			    if (cellPosition.Column == column && cellPosition.Row == row)
				    return control;
		    }
		    return null;
	    }
	}
}
