﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ABRSMPracticalTestTool.Data;

namespace ABRSMPracticalTestTool.Forms
{
    public partial class ScalesSetup : Form
    {
        private Form1 _mainForm;
	    private bool _minorModeIsSelected = false;

        public ScalesSetup(Form1 mainForm)
        {
            InitializeComponent();
            _mainForm = mainForm;
        }

        private void bConfirm_Click(object sender, EventArgs e)
        {
	        var grade = _mainForm.SelectedGrade;

	        if (grade < 3)
	        {
		        if (!radioNaturalMinor.Checked && !radioMelodicMinor.Checked && !radioHarmonicMinor.Checked)
		        {
			        MessageBox.Show("Come on, choose one. Just any of these 3. You need to practice to pass! Either that, or I will just take it that you pressed the button by mistake.", "Missing Selection");
			        return;
		        }
			}
	        else
	        {
		        if (!radioMelodicMinor.Checked && !radioHarmonicMinor.Checked)
		        {
			        MessageBox.Show("Come on, choose one. It's just one or the other, NOT both.", "Missing Selection");
			        return;
		        }
			}

	        if (radioNaturalMinor.Checked)
	        {
		        _mainForm.SelectedMinorScaleMode = EnumUtil.ScaleMode.NATURAL_MINOR;
	        }
	        else if (radioMelodicMinor.Checked)
	        {
		        _mainForm.SelectedMinorScaleMode = EnumUtil.ScaleMode.MELODIC_MINOR;
			}
	        else if (radioHarmonicMinor.Checked)
	        {
		        _mainForm.SelectedMinorScaleMode = EnumUtil.ScaleMode.HARMONIC_MINOR;
	        }

	        _minorModeIsSelected = true;

			Close();
        }

		private void ScalesSetup_Load(object sender, EventArgs e)
		{
			var grade = _mainForm.SelectedGrade;

			DisableRadioByGrade(grade);
		}

	    private void DisableRadioByGrade(int grade)
	    {
		    if (grade > 2)
		    {
			    radioNaturalMinor.Enabled = false;
		    }
	    }

		private void ScalesSetup_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!_minorModeIsSelected) Application.Exit();
		}
	}
}
