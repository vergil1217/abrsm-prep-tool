﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ABRSMPracticalTestTool.Forms
{
    public partial class GradeSelectionForm : Form
    {
        private Form1 _mainForm;
	    private bool _isGradeSelected = false;

        public GradeSelectionForm(Form1 mainForm)
        {
            InitializeComponent();
            _mainForm = mainForm;
        }

        private void GradeSelectionForm_Load(object sender, EventArgs e)
        {
            for (var i = 1; i <= 8; i++)
            {
                cbGradeSelection.Items.Add(i.ToString());
            }
        }

        private void bSelectGrade_Click(object sender, EventArgs e)
        {
	        if (cbGradeSelection.SelectedItem == null)
	        {
		        MessageBox.Show("Select a Grade! Otherwise I'll choose Grade 0.5 for you.", "Grade Not Selected");
		        return;
	        }

            _mainForm.SelectedGrade = int.Parse(cbGradeSelection.SelectedItem.ToString());
	        _isGradeSelected = true;

            Close();
        }

		private void GradeSelectionForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if(!_isGradeSelected) Application.Exit();
		}
	}
}
