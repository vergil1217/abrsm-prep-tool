﻿namespace ABRSMPracticalTestTool.Forms
{
    partial class ScalesSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.bConfirm = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.radioNaturalMinor = new System.Windows.Forms.RadioButton();
			this.radioMelodicMinor = new System.Windows.Forms.RadioButton();
			this.radioHarmonicMinor = new System.Windows.Forms.RadioButton();
			this.SuspendLayout();
			// 
			// bConfirm
			// 
			this.bConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.bConfirm.Location = new System.Drawing.Point(385, 300);
			this.bConfirm.Name = "bConfirm";
			this.bConfirm.Size = new System.Drawing.Size(205, 69);
			this.bConfirm.TabIndex = 3;
			this.bConfirm.Text = "Submit";
			this.bConfirm.UseVisualStyleBackColor = true;
			this.bConfirm.Click += new System.EventHandler(this.bConfirm_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(951, 37);
			this.label1.TabIndex = 4;
			this.label1.Text = "For your grade\'s Minor scales, you can choose from the following:";
			// 
			// radioNaturalMinor
			// 
			this.radioNaturalMinor.AutoSize = true;
			this.radioNaturalMinor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioNaturalMinor.ForeColor = System.Drawing.Color.White;
			this.radioNaturalMinor.Location = new System.Drawing.Point(371, 98);
			this.radioNaturalMinor.Name = "radioNaturalMinor";
			this.radioNaturalMinor.Size = new System.Drawing.Size(237, 40);
			this.radioNaturalMinor.TabIndex = 5;
			this.radioNaturalMinor.TabStop = true;
			this.radioNaturalMinor.Text = "Natural Minor";
			this.radioNaturalMinor.UseVisualStyleBackColor = true;
			// 
			// radioMelodicMinor
			// 
			this.radioMelodicMinor.AutoSize = true;
			this.radioMelodicMinor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioMelodicMinor.ForeColor = System.Drawing.Color.White;
			this.radioMelodicMinor.Location = new System.Drawing.Point(371, 144);
			this.radioMelodicMinor.Name = "radioMelodicMinor";
			this.radioMelodicMinor.Size = new System.Drawing.Size(243, 40);
			this.radioMelodicMinor.TabIndex = 6;
			this.radioMelodicMinor.TabStop = true;
			this.radioMelodicMinor.Text = "Melodic Minor";
			this.radioMelodicMinor.UseVisualStyleBackColor = true;
			// 
			// radioHarmonicMinor
			// 
			this.radioHarmonicMinor.AutoSize = true;
			this.radioHarmonicMinor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioHarmonicMinor.ForeColor = System.Drawing.Color.White;
			this.radioHarmonicMinor.Location = new System.Drawing.Point(371, 190);
			this.radioHarmonicMinor.Name = "radioHarmonicMinor";
			this.radioHarmonicMinor.Size = new System.Drawing.Size(271, 40);
			this.radioHarmonicMinor.TabIndex = 7;
			this.radioHarmonicMinor.TabStop = true;
			this.radioHarmonicMinor.Text = "Harmonic Minor";
			this.radioHarmonicMinor.UseVisualStyleBackColor = true;
			// 
			// ScalesSetup
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(986, 393);
			this.Controls.Add(this.radioHarmonicMinor);
			this.Controls.Add(this.radioMelodicMinor);
			this.Controls.Add(this.radioNaturalMinor);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.bConfirm);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ScalesSetup";
			this.ShowIcon = false;
			this.Text = "Scales Settings";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScalesSetup_FormClosing);
			this.Load += new System.EventHandler(this.ScalesSetup_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bConfirm;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton radioNaturalMinor;
		private System.Windows.Forms.RadioButton radioMelodicMinor;
		private System.Windows.Forms.RadioButton radioHarmonicMinor;
	}
}